﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace automatyKom
{
    class Cell
    {
        public Rectangle body;
        public static int idCount = 0;
        public int positionX;
        public int positionY;
        public int value;
        public int size;
        public bool changeStateFlag;
        public bool onBorder;
        public Cell neighbour;
        public int neighbourId;
        public SolidColorBrush neighbourColor;
        public int id;
        public SolidColorBrush color;
        static Random rand = new Random();

        public Cell(SolidColorBrush color, int size, int coordX, int coordY, int val)
        {
            this.body = new Rectangle();
            this.body.Width = size;
            this.body.Height = size;
            this.changeStateFlag = false;
            this.id = 0;
            this.positionX = coordX;
            this.positionY = coordY;
            this.body.Fill = color;
            this.value = val;
            this.size = size;
            this.onBorder = false;
            this.body.MouseLeftButtonDown += (source, e) =>
            {
                onMouseClick(false);
            };
        }

        public void onMouseClick(bool reclistalisation)
        {
            if (this.value == 0)
            {
                this.value = 1;
            }
            else if (this.value == 1)
            {
                setColor(Brushes.LightGray);
                this.value = 0;
            }

            if (this.value == 0)
            {
                Cell.idCount--;
                this.id = 0;
            }
            else
            {
                Cell.idCount++;
                this.id = Cell.idCount;
                int R = rand.Next(0, 255);
                int G = rand.Next(0, 255);
                int B = rand.Next(0, 255);
                this.color = new SolidColorBrush(Color.FromArgb(255, (Byte)(R), (Byte)(G), (Byte)(B)));
                setColor(this.color);
            }
        }

        public void setValue(int value)
        {
            this.value = value;
            if (value == 1)
            {
                setColor(this.color);

            }
            else if (value == 0)
            {
                setColor(Brushes.LightGray);
            }
            else
                setColor(Brushes.Red);
        }

        public void setColor(Brush color)
        {
            this.body.Fill = color;
        }

        public void setChangeStateFlag(int id, SolidColorBrush color)
        {
            this.neighbourId = id;
            this.neighbourColor = color;
            this.changeStateFlag = true;
        }

        public void changeState()
        {
            this.id = this.neighbourId;
            this.color = this.neighbourColor;
            setColor(this.color);
        }

        public void resetState()
        {
            this.changeStateFlag = false;
            neighbour = null;
            neighbourId = 0;
            neighbourColor = null;
        }
    }
}
