﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace automatyKom
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool mainThreadStopped;
        int iteration;
        int N;
        Thread mainThread;
        Engine engine;
        bool stopFlag;
        double speed;
        int cellSize;
        int threadCunter;

        public MainWindow()
        {
            mainThreadStopped = false;
            InitializeComponent();
            iteration = Int32.Parse(textboxIter.Text);
            stopFlag = false;
            speed = Double.Parse(textboxSpeed.Text);
            cellSize = Int32.Parse(comboBoxSize.Text);
            mainThread = new Thread(threadMethod);
            N = (int)canvas.Width / cellSize;
            if (labelMeshSize != null)
                labelMeshSize.Content = N.ToString() + "x" + N.ToString();
            
            engine = new Engine();
            engine.firstCount(N, cellSize);
            drawMesh();
           
            threadCunter = 0;
            Closing += this.OnWindowClosing;
        }

        private void resetEngine()
        {
            Cell.idCount = 0;
            iteration = Int32.Parse(textboxIter.Text);
            cellSize = Int32.Parse(comboBoxSize.Text);
            speed = Double.Parse(textboxSpeed.Text);
            N = (int)canvas.Width / cellSize;
            if (labelMeshSize != null)
                labelMeshSize.Content = N.ToString() + "x" + N.ToString();
            engine = new Engine();
            //stopFlag = true;
            iterationCountLabel.Content = "0";
            threadCunter = 0;
            // mainThread = new Thread(threadMethod);
            engine.firstCount(N, cellSize);
            drawMesh();
        }

        private void drawMesh()
        {
            canvas.Children.Clear();
            for (int j = 0; j < N; j++)
            {
                for (int i = 0; i < N; i++)
                {
                    Canvas.SetLeft(engine.cells[i, j].body, engine.cells[i, j].size * engine.cells[i, j].positionX);
                    Canvas.SetTop(engine.cells[i, j].body, engine.cells[i, j].size * engine.cells[i, j].positionY);
                    canvas.Children.Add(engine.cells[i, j].body);
                }
            }
        }

        private void startBtn_Click(object sender, RoutedEventArgs e)
        {
            iteration = Int32.Parse(textboxIter.Text);
            if (mainThread.ThreadState == System.Threading.ThreadState.Unstarted)
                mainThread.Start();
            else if (mainThread.ThreadState == System.Threading.ThreadState.Suspended)
                mainThread.Resume();
        }

        private void resetBtn_Click(object sender, RoutedEventArgs e)
        {
            resetEngine();
        }

        private void stopBtn_Click(object sender, RoutedEventArgs e)
        {
            if (mainThread.ThreadState != System.Threading.ThreadState.Unstarted && !mainThreadStopped)
                mainThread.Suspend();
        }

        private void threadMethod()
        {
            while (threadCunter <= iteration && !stopFlag)
            {
                Thread.Sleep(TimeSpan.FromSeconds(speed));
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                (ThreadStart)delegate ()
                {
                    engine.randomCellsExploring(N,engine.checkNeighbous);
                    engine.changeCellsState();
                    //engine.writeIds(N);
                    iterationCountLabel.Content = threadCunter.ToString();
                    threadCunter++;
                });
            }
            Console.WriteLine("Thread terminating gracefully.");
            mainThreadStopped = true;
        }

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            if (mainThread.ThreadState == System.Threading.ThreadState.Suspended)
                mainThread.Resume();
            stopFlag = true;
            Console.WriteLine("Program terminated");
        }
    }
}
