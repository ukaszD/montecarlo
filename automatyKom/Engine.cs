﻿using automatyKom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using static automatyKom.Cell;

class Engine
{
    class Coord
    {
        public int X;
        public int Y;
        private bool _old;
        public bool old
        {
            get
            {
                return _old;
            }
            set
            {
                _old = value;
            }
        }

        public Coord(int x, int y)
        {
            this.X = x;
            this.Y = y;
            this.old = false;
        }

        public Coord()
        {
            this.old = false;
        }
    }

    public Cell[,] cells;

    public Engine()
    {
    }

    public void isOnBorder(int i, int j, int maxI, int minI, int maxJ, int minJ)
    {
        if (cells[minI, minJ].id != cells[i, j].id && cells[minI, minJ].id != 0) //1
        {
            cells[i, j].onBorder = true;
        }
        else if (cells[i, minJ].id != cells[i, j].id && cells[i, minJ].id != 0) //2
        {
            cells[i, j].onBorder = true;
        }
        else if (cells[maxI, minJ].id != cells[i, j].id && cells[maxI, minJ].id != 0) //3
        {
            cells[i, j].onBorder = true;
        }
        else if (cells[minI, j].id != cells[i, j].id && cells[minI, j].id != 0) //4
        {
            cells[i, j].onBorder = true;
        }
        else if (cells[maxI, j].id != cells[i, j].id && cells[maxI, j].id != 0) //5
        {
            cells[i, j].onBorder = true;
        }
        else if (cells[minI, maxJ].id != cells[i, j].id && cells[minI, maxJ].id != 0) //6
        {
            cells[i, j].onBorder = true;
        }
        else if (cells[i, maxJ].id != cells[i, j].id && cells[i, maxJ].id != 0) //7
        {
            cells[i, j].onBorder = true;
        }
        else if (cells[maxI, maxJ].id != cells[i, j].id && cells[maxI, maxJ].id != 0) //8
        {
            cells[i, j].onBorder = true;
        }
        else
        {
            cells[i, j].onBorder = false;
        }
    }

    static Random random = new Random();

    public delegate void DelegatedMethod(int x, int y, int N);

    public void randomCellsExploring(int n, DelegatedMethod doOnPosition)
    {
        Random rand = new Random();
        List<Coord> positions = new List<Coord>();

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                positions.Add(new Coord(i, j));
            }
        }

        while (positions.Count != 0)
        {
            int randIndex = rand.Next(positions.Count);
            doOnPosition(positions[randIndex].X, positions[randIndex].Y, n);
            positions.RemoveAt(randIndex);
        }
    }

    public void setNewSeed(int x, int y, int N)
    {
        cells[x, y].onMouseClick(false);
    }

    public void getAllSeedBorders(int N)
    {
        for (int j = 0; j < N; j++)
        {
            for (int i = 0; i < N; i++)
            {
                if (i > 0 && i < N - 1 && j > 0 && j < N - 1)
                    isOnBorder(i, j, i + 1, i - 1, j + 1, j - 1);
                else
                    isOnBorder(i, j, getMod(i + 1, N), getMod(i - 1, N), getMod(j + 1, N), getMod(j - 1, N));
            }
        }
    }

    public void firstCount(int N, int cellSize)
    {
        cells = new Cell[N, N];

        for (int j = 0; j < N; j++)
        {
            for (int i = 0; i < N; i++)
            {
                cells[i, j] = new Cell(Brushes.LightGray, cellSize, i, j, 0);
            }
        }

        randomCellsExploring(N, setNewSeed);
    }

    public void resetCells()
    {
        foreach (Cell cell in cells)
        {
            cell.resetState();
        }
    }

    int getMod(int x, int N)
    {
        int result = (Math.Abs(x * N) + x) % N;
        return result;
    }

    public void checkNeighbous(int i, int j, int N)
    {
        if (i > 0 && i < N - 1 && j > 0 && j < N - 1)
            isOnBorder(i, j, i + 1, i - 1, j + 1, j - 1);
        else
            isOnBorder(i, j, getMod(i + 1, N), getMod(i - 1, N), getMod(j + 1, N), getMod(j - 1, N));

        if (cells[i, j].onBorder)
        {
            int maxI;
            int minI;
            int maxJ;
            int minJ;

            if (i > 0 && i < N - 1 && j > 0 && j < N - 1)
            {
                maxI = i + 1;
                minI = i - 1;
                maxJ = j + 1;
                minJ = j - 1;
            }
            else
            {
                maxI = getMod(i + 1, N);
                minI = getMod(i - 1, N);
                maxJ = getMod(j + 1, N);
                minJ = getMod(j - 1, N);
            }


            List<Cell> neighbours = new List<Cell>();

            neighbours.Add(cells[minI, minJ]);
            neighbours.Add(cells[i, minJ]);
            neighbours.Add(cells[maxI, minJ]);
            neighbours.Add(cells[minI, j]);
            neighbours.Add(cells[maxI, j]);
            neighbours.Add(cells[minI, maxJ]);
            neighbours.Add(cells[i, maxJ]);
            neighbours.Add(cells[maxI, maxJ]);

            int energy = 0;

            foreach (Cell cell in neighbours)
            {
                if (cell.id != cells[i, j].id)
                {
                    energy++;
                }
            }

            Cell tmpCenterCell = neighbours[random.Next(0, 8)];
            int secondEnergy = 0;

            foreach (Cell cell in neighbours)
            {
                if (cell.id != tmpCenterCell.id)
                {
                    secondEnergy++;
                }
            }

            if (energy - secondEnergy > 0) // energy > secondEnergy 
            {
                cells[i, j].setChangeStateFlag(tmpCenterCell.id, tmpCenterCell.color);
            }
        }
    }

    public void changeCellsState()
    {
        foreach (Cell cell in cells)
        {
            if (cell.changeStateFlag)
            {
                cell.changeState();
                cell.resetState();
            }
        }
    }
   

    public void writeIds(int N)
    {
        Console.WriteLine("");
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                Console.Write(cells[j, i].id + " ");
            }
            Console.WriteLine("");
        }
    }

}

